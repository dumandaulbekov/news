import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news/news.component';
import { NewsPaginationComponent } from './news-pagination/news-pagination.component';
import { CreateNewsComponent } from './news/create-news/create-news.component';

const routes: Routes = [
  { path: 'lazy-loading-news', component: NewsComponent },
  { path: 'create-news', component: CreateNewsComponent },
  { path: 'pagination', component: NewsPaginationComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
