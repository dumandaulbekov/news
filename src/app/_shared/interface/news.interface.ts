export interface INews {
  id?: number;
  title: string;
  description: string;
  imageUrl: string;
  content: string;
}
