import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { INews } from '../interface/news.interface';
import { PageEvent } from '@angular/material/paginator';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  private url = 'http://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) {}

  public getNews(params: PageEvent) {
    return this.http.get<[]>(
      `${this.url}?_page=${params.pageIndex}&_limit=${params.pageSize}`
    );
  }

  public getNewsFetch(params: PageEvent) {
    return fetch(
      `${this.url}?_page=${params.pageIndex}&_limit=${params.pageSize}`
    );
  }

  public createNews(news: INews): Observable<INews> {
    return this.http.post<INews>(`${this.url}`, news);
  }
}
