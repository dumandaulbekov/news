import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NewsService } from 'src/app/_shared/services/news.service';
import { INews } from 'src/app/_shared/interface/news.interface';

@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.scss'],
})
export class CreateNewsComponent implements OnInit {
  public newsForm: FormGroup;
  public disabled = false;

  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.newsForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(120),
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(50),
        Validators.maxLength(256),
      ]),
      imageUrl: new FormControl('', [Validators.required]),
      content: new FormControl('', [
        Validators.required,
        Validators.minLength(256),
      ]),
    });
  }

  public createNews(): void {
    if (this.newsForm.invalid) {
      return;
    }

    const news: INews = {
      title: this.newsForm.value.title,
      description: this.newsForm.value.description,
      imageUrl: this.newsForm.value.imageUrl,
      content: this.newsForm.value.content,
    };

    this.disabled = true;
    this.newsService.createNews(news).subscribe(() => {
      console.log('created', news);

      this.disabled = false;
      this.newsForm.reset();
    });
  }
}
