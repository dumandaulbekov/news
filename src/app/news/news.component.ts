import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener,
} from '@angular/core';
import { NewsService } from '../_shared/services/news.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  public newsFeed = [];
  public loaded = false;

  private pageIndex = 1;
  private pageSize = 3;
  private newsHeight = 500;
  @ViewChild('newsHeightEl') newsHeightEl: ElementRef;

  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.news({ pageIndex: this.pageIndex, pageSize: this.pageSize, length });
  }

  @HostListener('window:scroll') lazyLoadingNews() {
    if (pageYOffset >= this.newsHeight) {
      this.pageIndex++;

      this.newsHeight = this.newsHeightEl.nativeElement.offsetHeight - 300;
      this.news({ pageIndex: this.pageIndex, pageSize: this.pageSize, length });

      // console.log('news height', this.newsHeight);
      // console.log('pageIndex', this.pageIndex);
      return;
    }
    // console.log('pageYOffset', pageYOffset);
  }

  private news(params: PageEvent) {
    console.log('call');
    this.loaded = true;
    this.newsService.getNews(params).subscribe((feed) => {
      feed.forEach((news) => {
        this.newsFeed.push(news);
      });

      this.loaded = false;

      console.log('news', this.newsFeed);
      console.log('-----');
    });
  }

  public onUpPage() {
    window.scrollTo(0, 0);
  }
}
