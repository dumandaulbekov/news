import { Component, OnInit } from '@angular/core';
import { NewsService } from '../_shared/services/news.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-news-pagination',
  templateUrl: './news-pagination.component.html',
  styleUrls: ['./news-pagination.component.scss'],
})
export class NewsPaginationComponent implements OnInit {
  newsFeed = [];
  newsLength: number;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [3, 5, 10];

  public loaded = true;
  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.news({
      previousPageIndex: 0,
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      length,
    });
  }

  news(params: PageEvent) {
    params.pageIndex++;
    params.previousPageIndex++;
    console.log('params', params);

    this.loaded = false;
    this.newsService
      .getNewsFetch(params)
      .then(async (response) => {
        this.newsLength = 100;
        this.newsFeed = await response.json();
      })
      .catch((error) => console.log('err', error))
      .finally(() => {
        window.scrollTo(0, 0);
        this.loaded = true;
      });
  }
}
